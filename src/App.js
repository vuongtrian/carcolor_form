import React from "react";
import "./App.css";
import CarExercise from './CarExercise';


function App() {
  return (
    <div className="App">
      <CarExercise/>
    </div>
  );
}

export default App;
